from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.quiz, name="quiz"),
    path('welcome', views.welcome, name="welcome"),
    path('saveans', views.saveans, name="saveans"),
    path('result', views.result, name="result"),

    # path('save_ans/', views.save_ans, name="saveans")
]
