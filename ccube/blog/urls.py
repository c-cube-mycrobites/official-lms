
from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.allblogs, name="allblogs"),
    path('enrollments', views.blogHome, name="bloghome"),
    path('simulations', views.simulations, name="simulations"),
    #path('quiz', views.quiz, name="quiz"),
    path('video', views.video, name='video'),
    path('forum', views.forum, name='forum'),
    path('payment', views.payment, name='payment'),
    path('success', views.success, name='success'),
    path('review', views.review, name='review'),
    path('enroll/<slug:slug>', views.enroll, name='enroll'),
    path('deroll/<slug:slug>', views.deroll, name='deroll'),
    path('check_enroll/<slug:slug>', views.check_enroll, name='check_enroll'),
    path('<str:slug>', views.blogPost, name="blogPost"),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
