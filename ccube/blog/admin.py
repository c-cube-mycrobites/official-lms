from django.contrib import admin
from blog.models import Post, AssignCourses, SideBar, Review
import nested_admin
admin.site.register(AssignCourses)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    class Media:
        js = ('tineInject.js',)


@admin.register(SideBar)
class SideBarAdmin(admin.ModelAdmin):
    class Media:
        js = ('tineInject.js',)
@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    class Media:
        js = ('tineInject.js',)
