from django.shortcuts import render, HttpResponse, redirect
from blog.models import Post, AssignCourses, Review
from django.contrib import messages
from django.contrib.auth.models import User
from blog.templatetags import extras
from django.contrib.auth.decorators import login_required
import razorpay
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def allblogs(request):
    allPosts = Post.objects.all()

    context = {'allPosts': allPosts}
    if request.method == "POST":
        name = request.POST.get('name')
        amount = 50000

        client = razorpay.Client(
            auth=("rzp_test_AOEptellJQ7gKp", "lgclIkNTL6wpFbqh7jK2Hoft"))

        payment = client.order.create({'amount': amount, 'currency': 'INR',
                                       'payment_capture': '1'})
    return render(request, "blog/allblogs.html", context)


def blogHome(request):
    allPosts = AssignCourses.objects.filter(user=request.user)

    context = {'allPosts': allPosts}
    return render(request, "blog/blogHome.html", context)


def blogPost(request, slug):
    post = Post.objects.filter(slug=slug).first()
    post.views = post.views + 1
    post.save()
    context = {'post': post, 'slug': slug}
    return render(request, "blog/blogPost.html", context)


@login_required
def enroll(request, slug):
    this_event = Post.objects.get(slug=slug)
    this_event.enroll(user=request.user)

    messages.success(request, "You have successfully enrolled the course.")
    return redirect("bloghome")


@login_required
def deroll(request, slug):
    this_event = Post.objects.get(slug=slug)
    this_event.deroll(user=request.user)
    messages.error(request, "You have successfully unenrolled the course.")
    return redirect("bloghome")


def check_enroll(request, slug):
    try:
        this_event = Post.objects.get(slug=slug)
        registered = AssignCourses.objects.get(
            user=request.user, post=this_event)
        if registered:
            return True
    except:
        return False


def simulations(request):
    slugss = request.GET["post2"]
    print(slugss)
    post = Post.objects.filter(slug=slugss).first()
    context = {'post': post, 'slugss': slugss}
    return render(request, "blog/simulations.html", context)


def video(request):
    slugs = request.GET["post"]
    post = Post.objects.filter(slug=slugs).first()
    context = {'post': post, 'slugs': slugs}
    return render(request, "blog/video.html", context)


def review(request):

    # return HttpResponse("This is cntact page!")
    if request.method == "POST":
        name = request.POST['name']
        content1 = request.POST['content1']
        content2 = request.POST['content2']
        if len(name) < 2 or len(content1) < 4 or len(content2) < 4:
            messages.error(request, "Please fill the form correctly")
        else:
            review = Review(name=name, content1=content1, content2=content2)
            review.save()
            messages.success(
                request, "Your message has been successfully sent. Please go back to courses and continue your reading")

    return render(request, "blog/review.html")


def forum(request):
    return render(request, "blog/forum.html")


def payment(request):
    return render(request, "blog/payment.html")


@csrf_exempt
def success(request):
    # client = razorpay.Client(
    #         auth=("rzp_test_AOEptellJQ7gKp", "lgclIkNTL6wpFbqh7jK2Hoft"))
    # response = request.POST
    # # took this values from the razorpay payment form
    # params_dict = {
    #     'razorpay_payment_id': response.get('razorpay_payment_id',0),
    #     'razorpay_order_id': response.get('razorpay_order_id',0),
    #     'razorpay_signature': response.get('razorpay_signature',0),
    # }
    # status = client.utility.verify_payment_signature(params_dict)
    # print(status)
    # # jsondata = json.dumps(client.payment.fetch(params_dict['razorpay_payment_id']))
    # # dict_data = json.loads(jsondata)
    return render(request, "blog/blogHome.html")
