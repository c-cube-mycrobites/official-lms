from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    # path('createquiz', views.createquiz, name='createquiz'),
    path('createcourse', views.createcourse, name='createcourse'),
    path('joinus', views.joinus, name='joinus'),
    path('viewcourse', views.viewcourse, name='viewcourse'),
    path('<str:slug>', views.course, name="course"),

    path('signup/', views.signup, name='signup'),
]
