from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Contact(models.Model):
    sno = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=13)
    email = models.CharField(max_length=100)
    content = models.TextField()
    timeStamp = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return 'Message from ' + self.name


class Role(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(max_length=30, default="Student", null=False)


class Games(models.Model):
    sno = models.AutoField(primary_key=True)
    game_name = models.CharField(max_length=100)
    grade = models.CharField(max_length=50)
    image = models.ImageField(null=True, blank=True, upload_to="images/")
    total_levels = models.IntegerField()                                                                                                                               

class Game_link(models.Model):
    game_id = models.ForeignKey(Games, on_delete=models.CASCADE)
    level_number = models.IntegerField()
    level_url = models.URLField(max_length=400)
