from django.contrib import admin
from .models import Contact, Games, Game_link
# Register your models here.
admin.site.register(Contact)

admin.site.register(Games)
admin.site.register(Game_link)
