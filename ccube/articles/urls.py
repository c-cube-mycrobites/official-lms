from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.articleHome, name="articlehome"),
    path('create', views.create, name="create"),
    path('<str:slug>', views.articlePost, name="articlePost"),
]
