from django.shortcuts import render, HttpResponse, redirect
from articles.models import Article
from django.contrib import messages
from django.contrib.auth.models import User


# Create your views here.
def articleHome(request): 
    allPosts= Article.objects.all()
    context={'allPosts': allPosts}
    return render(request, "articles/blogHome.html", context)

def articlePost(request, slug): 
    post=Article.objects.filter(slug=slug).first()
    post.save()
    
    
    context={'post':post}
    return render(request, "articles/blogPost.html", context)



def create(request):
    if request.method=="POST":
        title=request.POST['title']
        content=request.POST['content']
        author=request.POST['author']
        slug =request.POST['slug']
        if len(title)<2 or len(content)<3 :
            messages.error(request, "Please fill the form correctly")
        else:
            posts=Article(title=title, content=content, author=author, slug=slug)
            posts.save()
            messages.success(request, "Your message has been successfully sent")
    return render(request, "articles/create.html")


