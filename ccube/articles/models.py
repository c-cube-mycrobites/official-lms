from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

# Create your models here.


class Article(models.Model):
    sno = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    content = models.TextField()
    image = models.ImageField(null=True, blank=True, upload_to="images/")
    author = models.CharField(max_length=50)
    slug = models.CharField(max_length=130)

    def __str__(self):
        return self.title + 'by' + self.author
